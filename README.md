# EpiTint
EpiTint: An Automatized and Integrative Bioinformatics Approach to Identify and Visualize Differentially Methylated Regions and Cytosines

## Requirements

## Installation

For installation, first clone the repository. You can install all the dependencies directly using conda:

```
conda create --name myenv --file environment.yml
```

Then, clone the TrimGalore and Bismark repositories in the software epitint repository

```
cd software
git clone https://github.com/FelixKrueger/Bismark.git
git clone https://github.com/FelixKrueger/TrimGalore.git
```

## Module 1: Quality control and data trimming

The workflow commenced with a sample coverage check as the first step to evaluate the sequencing quality. 

- Input: directory with raw paired-end fastq files.
- Output: directory with trimmed paired-end fastq files and quality control information.

Example of use:

In `software`:

```
bash software/module1/preprocessing.sh -i raw_data -o trimmed -q 20 -l 80 -j threads
```
In `software/module1/quality_control`:

```
bash software/module1/quality_control/quality_control.sh -p output_dir -i fastqs_rawfolder -j trimmed -t threads
```

The software used in this part is the following one:

```
software/
├── preprocessing.sh
└── quality_control
    ├── adapters.fa
    ├── basequality.py
    ├── fastQC_table.py
    ├── fastq_unifier.py
    ├── output.txt
    ├── quality_control.sh
    ├── report.py
    └── resumeplot.py
```

The following code chunk shows an example of the output files that are generated during this step.

```
Example of quality control output files
QC/
├── barcode-primers_report.pdf
├── fastq_stats.xlsx
├── fastqc
│   ├── postprocessed
│   │   ├── file_R1_001_val_1_fastqc
│   │   ├── file_R1_001_val_1_fastqc.html
│   │   ├── file_R1_001_val_1_fastqc.zip
│   │   ├── file_R2_001_val_2_fastqc
│   │   ├── file_R2_001_val_1_fastqc.html
│   │   ├── file_R2_001_val_1_fastqc.zip
│   │   ├── FAILED_GC-content.txt
│   │   ├── FAILED_adapter-content.txt
│   │   ├── FAILED_basic-statistics.txt
│   │   ├── FAILED_per-base-N-content.txt
│   │   ├── FAILED_per-base-sequence-quality.txt
│   │   ├── FAILED_per-sequence-queality.txt
│   │   ├── FAILED_sequence-length-distribution.txt
│   │   ├── fastqs_stats.txt
│   │   ├── resume.txt
│   │   └── summary.txt
│   └── preprocessed
│       ├── file_R1_001_fastqc
│       ├── file_R1_001_fastqc.html
│       ├── file_R1_001_fastqc.zip
│       ├── file_R2_001_fastqc
│       ├── file_R2_001_fastqc.html
│       ├── file_R2_001_fastqc.zip
│       ├── FAILED_GC-content.txt
│       ├── FAILED_adapter-content.txt
│       ├── FAILED_basic-statistics.txt
│       ├── FAILED_per-base-N-content.txt
│       ├── FAILED_per-base-sequence-quality.txt
│       ├── FAILED_per-sequence-queality.txt
│       ├── FAILED_sequence-length-distribution.txt
│       ├── fastqs_stats.txt
│       ├── resume.txt
│       └── summary.txt
├── images.fof
├── meanseqqual_sample.png
├── post-triming_counts.txt
├── postprocessed-resume.png
├── posttrimming.fof
├── pre-triming_counts.txt
├── preprocessed-resume.png
└── pretrimming.fof
```

## Module 2: Processed read alignment

### Using Bismark as CX reporter

#### 1. **Genome preparation**

INPUT: reference genome
OUTPUT: bisulfated genomes

This command was used to index the genome and prepared it as a bisulfite-converted genome. Change the `Bismark/index` directory:

```
$ Bismark/bismark_genome_preparation –genome-composition --verbose Bismark/index/
```
**Important Note: Reference Genome Download**

To ensure the proper functionality of this software, we strongly recommend that you download or place the reference genome in the designated folder. 

**Instructions:**

1. **Download the Reference Genome**: Obtain the reference genome for your analysis. You may find it on official genome repositories or project-specific sources.

2. **Place the Genome Data**: Move or copy the downloaded reference genome files into the "Reference_Genome" folder.

3. **Update File Paths**: If necessary, make sure that the software or scripts you're using reference the correct path to the reference genome data. 

By following these steps, you'll help ensure that the software operates smoothly and that your analyses are accurate. If you have any questions or encounter issues, please refer to the documentation or contact our support team for assistance.

#### 2. Bismark alignment and remove duplicates

INPUT: trimmed fastq files
OUTPUT: bam files

Second step was aligning sequences using Bowtie2 for each fastQ paired-end file. The parameters modified in this command were:

* N (int): number of tolerating non-bisulfite mismatches per read.
* L (int): length of the seed substrings to align during multiseed alignment.

```
$ Bismark/bismark --parallel threads --bowtie2 -o outdir -N 1 -L 32 --genome_folder Bismark/index/ -1 ${sample_id}_1.fq.gz -2 ${sample_id}_2.fq.gz
$ picard MarkDuplicates INPUT=${sample_id} OUTPUT=${sample_id}.rmdp.bam METRICS_FILE=${sample_id}.rmdp.metrics.txt CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT REMOVE_DUPLICATES=true 2> ${sample_id}.err 1> ${sample_id}.out
```

#### 3. Bismark deduplication

INPUT: bam files
OUTPUT: deduplicated bam files

Third step was skipped because of the nature of the libraries (enrichment libraries). If you want to include it, an example command would be:

```
$ Bismark/deduplicate_bismark -p --output_dir outdir --bam ${sample_id}.rmdp.bam
```

#### 4. Bismark methylation extractor

```
$ Bismark/bismark_methylation_extractor --no_overlap --gzip --parallel threads --bedGraph --cytosine_report --genome_folder Bismark/index/ --CX -o outdir ${sample_id}.rmdp.bam
```

#### 5. Keep cytosines higher or equal than 10 

The coverage threshold can be modifiable as requiered. For the test data we recommend to set the coverage in 5.

INPUT: bismark.cov.gz files
OUTPUT: bismark_filt.cov.gz files

```
$ for i in *.bismark.cov.gz; do base=$(basename $i .cov.gz); zcat $i | awk '{if (($5 + $6) >= 10) {print $0};}' | gzip > ${base}_filt.cov.gz;done
```

#### 6. Cytosine coverage report

INPUT: bismark_filt.cov.gz files
OUTPUT: CX report files

```
$ Bismark/coverage2cytosine --genome_folder Bismark/index/ --CX_context --dir outdir -o outname cov_file_filt
```
## Module 3: Cytosine clustering and DMR annotation

INPUT: import_data.R filled in the `software` folder and CX report files

An example of import_data.R (saved in `software`):

```
# import_data.R

# Load the DMRcaller library
library("DMRcaller")

# Read bismark CX_report files
path <- "CX_report/"

# Control group
control1 <- readBismark(paste0(path, "test1_1_val_1_bismark_bt2_pe.bismark_filt.cov.gz.CX_report.txt"))
control2 <- readBismark(paste0(path, "test2_1_val_1_bismark_bt2_pe.bismark_filt.cov.gz.CX_report.txt"))

# Case group
case1 <- readBismark(paste0(path, "test3_1_val_1_bismark_bt2_pe.bismark_filt.cov.gz.CX_report.txt"))
case2 <- readBismark(paste0(path, "test4_1_val_1_bismark_bt2_pe.bismark_filt.cov.gz.CX_report.txt"))

# Create the methylationDataList
methylationDataList <- GRangesList(
  "control" = c(control1, control2),
  "case" = c(case1, case2)
)
```

Run DMRcaller for the cytosine clustering:

```
Rscript software/DMRcaller.R control_folder case_folder
```

Run DMR annotation in order to annotate the DMR:

```
python EpiTint_Module3_annotation.py DMRcaller_output_tsv_folder/ --download_gtf
```

## Module 4: DMR Visualization

INPUT: 
- CX reports divided in control / case folders (output from Module 2)
- Annotation file (output from Module 3) 
- Genomic features

OUTPUT: PDF file with the plot

Example of use:

```
python EpiTint_Module4_DMR.py --annotation_file annotation_file --control_folder control_folder --case_folder case_folder --gene_name "IRS2" --feature 13:109752695-109756308,13:109782042-109786583 --first_feature 1 --mean_diff_value 10 --output_dir output --flanked 50 --dpi 600
```

| Parameter          | Type    | Description                                                 | Default Value |
|--------------------|---------|-------------------------------------------------------------|---------------|
| `--annotation_file`| str     | Path to annotation file in TSV format (`file.tsv`)         |               |
| `--control_folder` | str     | Path to control folder (`input_folder`)                     |               |
| `--case_folder`    | str     | Path to case folder (`input_folder`)                        |               |
| `--gene_name`      | str     | Gene name ("IRS2")                                          | ""            |
| `--features`       | str     | List of coordinates (`1:109784400-109785600,1:109784400-109785600`) |       |
| `--first_feature`  | int     | Value to control the first feature splitting (i.e. 2)       | 0             |
| `--last_feature`   | int     | Value to control the last feature splitting (i.e. 2)        | 0             |
| `--mean_diff_value`| int     | Mean difference value (i.e. 5)                              | 0             |
| `--is_region`      | boolean | Represent a genomic region                                   |               |
| `--flanked`        | int     | Exon flanked for each side (i.e. 50)                        | 0             |
| `--output_dir`     | str     | Output directory                                            |               |
| `--reverse_strand` | boolean | Strand direction                                            |               |
| `--width`          | int     | Plot width                                                  | 10            |
| `--height`         | int     | Plot height                                                 | 1.5           |
| `--dpi`            | int     | Plot dpi                                                    | 300           |


# Snakemake Epitint

You can use `snakemake` to run the pipeline as follows:

```

```

# Results:
First approach:
![Image1](images/1stapproach.png)


Second approach:
![Image2](images/2ndapproach.png)


Third approach:
![Image3](images/3rdapproach.png)

